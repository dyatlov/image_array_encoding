package image_array_encoding

import (
	"io"
	"log"
	"testing"

	"code.google.com/p/go-uuid/uuid"
)

func TestNewImzmlReader(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}

	observedId := reader.Id()
	expectedId := uuid.Parse("A85C18E3-AB63-40BC-ABD7-0EB5BDC29CE4")
	if observedId.String() != expectedId.String() {
		t.Errorf("reader.Id() = %v, expected = %v", observedId, expectedId)
	}
}

func TestMzAxis(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}
	var firstExpected float32 = 225.0
	var lastExpected float32 = 249.91667
	mzAxis := reader.MzAxis()
	if mzAxis[0] != firstExpected {
		t.Errorf("mzAxis[0] = %v, expected = %v", mzAxis[0], firstExpected)
	}
	if mzAxis[len(mzAxis)-1] != lastExpected {
		t.Errorf("mzAxis[0] = %v, expected = %v", mzAxis[len(mzAxis)-1],
			lastExpected)
	}
}

func TestNextIntensities(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal("%v", err)
	}
	for {
		intensities, err := reader.NextIntensities()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		}
		if int64(len(intensities)) != reader.MzAxisLen() {
			t.Errorf("len = %v, %v\n", len(intensities), intensities)
		}
	}
}

func TestMaxY(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}
	if reader.MaxX() != 180 {
		t.Errorf("reader.MaxX() == %v, expected %v", reader.MaxX(), 180)
	}
}

func TestMaxX(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}
	if reader.MaxY() != 90 {
		t.Errorf("reader.MaxY() == %v, expected %v", reader.MaxY(), 90)
	}
}

func TestMzAxisLen(t *testing.T) {
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}
	mzAxisLen := reader.MzAxisLen()
	const expected = 300
	if mzAxisLen != expected {
		t.Errorf("reader.MzAxisLen() == %v, expected %v", mzAxisLen, expected)
	}
}
