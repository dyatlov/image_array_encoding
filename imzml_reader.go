package image_array_encoding

import (
	"code.google.com/p/go-charset/charset"
	_ "code.google.com/p/go-charset/data"
	"code.google.com/p/go-uuid/uuid"
	"encoding/binary"
	"encoding/xml"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type ImzmlReader struct {
	id         uuid.UUID
	xmlName    string
	binName    string
	continuous bool
	bin        *os.File
	mzAxis     []float32
	mzAxisLen  int64
	maxX       int64
	maxY       int64
}

type params struct {
	Params []param `xml:"cvParam"`
}

type param struct {
	Name string `xml:"name,attr"`
	Val  string `xml:"value,attr"`
}

func NewImzmlReader(xmlName string, binName string) (r *ImzmlReader, err error) {
	r = new(ImzmlReader)
	r.xmlName = xmlName
	r.binName = binName

	if err = r.readMetadata(); err != nil {
		return nil, err
	}

	if err = r.readMzAxis(); err != nil {
		return nil, err
	}

	return r, nil
}

func (r *ImzmlReader) Id() uuid.UUID {
	return r.id
}

func (r *ImzmlReader) MzAxis() []float32 {
	return r.mzAxis
}

func (r *ImzmlReader) MzAxisLen() int64 {
	return r.mzAxisLen
}

func (r *ImzmlReader) MaxX() int64 {
	return r.maxX
}

func (r *ImzmlReader) MaxY() int64 {
	return r.maxY
}

func (r *ImzmlReader) NextIntensities() ([]float32, error) {
	var intensities = make([]float32, r.mzAxisLen)
	err := binary.Read(r.bin, binary.LittleEndian, intensities)
	if err != nil {
		if err == io.EOF {
			return intensities, io.EOF
		}
		return nil, fmt.Errorf("Cannot read intensities "+
			"from binary file, error: %v",
			err)
	}
	return intensities, nil
}

func (r *ImzmlReader) Close() error {
	err := r.bin.Close()
	if err != nil {
		return fmt.Errorf("Cannot close binary file, error: %v", err)
	}
	return nil
}

func (r *ImzmlReader) readMzAxis() (err error) {
	r.mzAxis = make([]float32, r.mzAxisLen)
	r.bin, err = os.Open(r.binName)
	if err != nil {
		return fmt.Errorf("Cannot open binary file, error: %v", err)
	}

	// Skip first 16 bytes as it is UUID
	_, err = r.bin.Seek(16, 0)
	if err != nil {
		return fmt.Errorf("Cannot read binary file, error: %v", err)
	}
	err = binary.Read(r.bin, binary.LittleEndian, r.mzAxis)
	if err != nil {
		return fmt.Errorf("Cannot read MzAxis from binary file, error: %v", err)
	}
	return nil
}

func (r *ImzmlReader) readMetadata() error {
	file, err := os.Open(r.xmlName)
	defer file.Close()
	if err != nil {
		return fmt.Errorf("Cannot open file, error: %v", err)
	}

	decoder := xml.NewDecoder(file)
	decoder.Strict = true
	decoder.CharsetReader = charset.NewReader

	for {
		token, err := decoder.Token()
		if token == nil {
			if err == io.EOF {
				return fmt.Errorf("Cannot find metadata")
			} else {
				return fmt.Errorf("Cannot parse xml file, error: %v", err)
			}
		}

		switch token.(type) {
		case xml.StartElement:
			element := token.(xml.StartElement)
			switch element.Name.Local {
			case "fileContent":
				var fileContent params
				decoder.DecodeElement(&fileContent, &element)
				for _, p := range fileContent.Params {
					switch p.Name {
					case "processed":
						r.continuous = false
						return fmt.Errorf("Unsupported format")
					case "continuous":
						r.continuous = true
					case "universally unique identifier":
						r.id = uuid.Parse(strings.Trim(p.Val, "{}"))
					}
				}
			case "scanSettings":
				var scanSettings params
				decoder.DecodeElement(&scanSettings, &element)
				for _, p := range scanSettings.Params {
					switch p.Name {
					case "max count of pixel x":
						r.maxX, err = strconv.ParseInt(p.Val, 10, 64)
						if err != nil {
							return fmt.Errorf("wrong max count of pixel x")
						}
					case "max count of pixel y":
						r.maxY, err = strconv.ParseInt(p.Val, 10, 64)
						if err != nil {
							return fmt.Errorf("wrong max count of pixel y")
						}
					}
				}
			case "binaryDataArray":
				var binaryArray params
				decoder.DecodeElement(&binaryArray, &element)
				for _, p := range binaryArray.Params {
					switch p.Name {
					case "external array length":
						r.mzAxisLen, err = strconv.ParseInt(p.Val, 10, 32)
						if err != nil {
							return fmt.Errorf("wrong external array length")
						}
						// Assume it's a last element
						return nil
					}
				}
			}
		}
	}
}
