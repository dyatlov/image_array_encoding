package image_array_encoding

import (
	"compress/lzw"
	"encoding/binary"
	"io"
	"log"
	"os"
	"testing"
)

// Number of spectra in one chunk

func TestWrite(t *testing.T) {
	const fileNmae = "plain.bin"
	plainFile, err := os.Create(fileNmae)
	if err != nil {
		log.Fatal(err)
	}
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}

	totalFloatsCounter := 0
	for {
		intensities, err := reader.NextIntensities()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		}
		err = binary.Write(plainFile, binary.LittleEndian, intensities)
		if err != nil {
			log.Fatal(err)
		}
		totalFloatsCounter += len(intensities)
	}

	plainFileInfo, err := plainFile.Stat()
	if err != nil {
		log.Fatal(err)
	}
	expected := 4860000
	if totalFloatsCounter != expected {
		t.Errorf("totalFloatsCounter = %v, expected  is %v", expected)
	}

	if int(plainFileInfo.Size()) != totalFloatsCounter*4 {
		t.Errorf("plainFileInfo.Size() = %v, expected  is %v",
			plainFileInfo.Size(),
			totalFloatsCounter*32)
	}
	log.Printf("Size of %v is %v KB, total number of floats written %v\n",
		plainFileInfo.Name(),
		plainFileInfo.Size()/1024,
		totalFloatsCounter)
}

func TestCompression(t *testing.T) {
	const fileNmae = "compressed.bin"
	const chunkSize = 77

	plainFile, err := os.Create(fileNmae)
	if err != nil {
		log.Fatal(err)
	}
	reader, err := NewImzmlReader(
		"S042_Continuous.imzML",
		"S042_Continuous.ibd")
	defer reader.Close()
	if err != nil {
		log.Fatal(err)
	}

	compressor := lzw.NewWriter(plainFile)
	defer compressor.Close()
	counter := 0
	chunk := make([]float32, 0, reader.MzAxisLen()*chunkSize)
	totalFloatsCounter := 0
	for {
		intensities, err := reader.NextIntensities()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		}
		chunk = append(chunk, intensities...)
		counter++
		if counter == chunkSize {
			err = binary.Write(compressor, binary.LittleEndian, chunk)
			if err != nil {
				log.Fatal(err)
			}
			totalFloatsCounter += len(chunk)
			chunk = chunk[:0]
			counter = 0
		}

	}
	if len(chunk) != 0 {
		binary.Write(compressor, binary.LittleEndian, chunk)
		totalFloatsCounter += len(chunk)
	}

	plainFileInfo, err := plainFile.Stat()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Size of %v is %v KB, total number of floats written %v, chunk size is %v\n",
		plainFileInfo.Name(),
		plainFileInfo.Size()/1024,
		totalFloatsCounter,
		chunkSize)
}
