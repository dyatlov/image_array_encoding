This is a testing stage for the different ways to transfer pseudo images to the web-client. At least the following ways should be discovered:

* JSON
* HTML5 bytearrays or blobs
* As a PNG image with encoding on the client

The following indicators should be measured:

* Raw data to transfer / data transfered in deed
* Decompression time